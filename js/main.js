/*OLOO objects linking other objects or object inheritance.

 At the time of creating this there werent any blogs explaining whats here but the base ideas are taken from "you dont
 know javascript" and Stampit. In the case of Stampit it was to find pattern using javascript that
 didnt need to download another module to get it to do it for you .It maybe the way forward or it
 might have holes poked in it by people that possibly know what they are talking about and just dont like it .....

 A small aside is that prototype is faster to create then Object.assign but its had years of optimisation in V8 etc

 MDN pages
 Object.create() https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/create
 Object.assign() https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
 */

function OLOO() {
    // The basic approach
    const basicApproach = Object.assign(Object.create({
        myFn(){
            return "test"
        }
    }), {MyParameters: "test"});

    console.log("The basic approach", basicApproach);


    function myParamPrivate(__value) {
        let privateValue = __value;
        return {
            getValue: () => privateValue,
            publicValue: []
        }
    }

    const myPrototype = {
        addToPublic (value) {
            this.publicValue.push(value);
        },
        getPublic () {
            return this.publicValue;
        }
    }

    const myOLOO = (value) => Object.assign(Object.create(myPrototype), myParamPrivate(value));

    let myFirstOLOO = myOLOO("test");

    console.log("myFirstOLOO", myFirstOLOO);


    //Extending parameters

    const moreUsefulCode = function (__value) {
        let privateValue = __value
        return {
            getMoreUsefulValue: () => privateValue
        }
    }

    const myOLOOExtended = (firstValue, extendedvalue) => Object.assign(myOLOO(firstValue), moreUsefulCode(extendedvalue));


    let myFirstOLOOExtended = myOLOOExtended("test", "extendedTest");

    console.log(`
    myParamPrivate: ${myFirstOLOOExtended.getValue()}
    moreUsefulCode: ${myFirstOLOOExtended.getMoreUsefulValue()}
    `);


    // "composing" paramiters

    const myOLOOComposed = (firstValue, extendedvalue) => Object.assign(Object.create(myPrototype), myParamPrivate(firstValue), moreUsefulCode(extendedvalue));

    let myFirstOLOOComposed = myOLOOComposed("test", "extendedTest");

    console.log(` composed-
    myParamPrivate: ${myFirstOLOOComposed.getValue()}
    moreUsefulCode: ${myFirstOLOOComposed.getMoreUsefulValue()}
    `);


    // bit tidier

    const composedParams = (firstValue, extendedvalue) => Object.assign( myParamPrivate(firstValue), moreUsefulCode(extendedvalue));

    const myOLOONeeterComposed = (firstValue, extendedvalue) => Object.assign(Object.create(myPrototype), composedParams(firstValue, extendedvalue));

    const myFirstOLOONeeterComposed = myOLOONeeterComposed("test", "extendedTest");

    console.log(` neeter-
    myParamPrivate: ${myFirstOLOONeeterComposed.getValue()}
    moreUsefulCode: ${myFirstOLOONeeterComposed.getMoreUsefulValue()}
    `);


    // I've left it as an object so you can see that when you extend it adds everywhere.

    let anotherPrototype = {
        getPublicLength () {
            return this.publicValue.length;
        }
    };

    console.log("pre extension", myPrototype.hasOwnProperty('getPublicLength'));

    const myPrototypeExtend = Object.assign(myPrototype, anotherPrototype);

    console.log("post extension", myPrototype.hasOwnProperty('getPublicLength'));


    const thefullmonty = (firstValue, extendedvalue) => Object.assign(myOLOONeeterComposed);

    const OLOOExtendedPrototype = myOLOONeeterComposed("first", "second");

    console.log(` neeter-
    myParamPrivate: ${OLOOExtendedPrototype.getValue()}
    moreUsefulCode: ${OLOOExtendedPrototype.getMoreUsefulValue()}
    getPublicLength: ${OLOOExtendedPrototype.getPublicLength()}
    myPrototypeExtendedEverywhere: ${(myFirstOLOONeeterComposed.getPublicLength)}
    `);


    // lets you see what was in the compose
    // how to use: compose( FnA, trace("tag Name"), FnB);
    let trace = _.curry(function (tag, x) {
        console.log(tag, x);

        return x;
    });

    const compose = stampit.compose;
    const init = stampit.init;





    // an example based on Maybe from the mostly-adequate-guide

    const MaybeProto = {
        map(fn) {
            return this.isNothing() ? this.of(null) : this.of(fn(this.__value));
        },

        isNothing() {
            return (this.__value === null || this.__value === undefined);
        },
        of(x) {
            return Maybe(x)
        }
    };

    const Maybe = (__value) => Object.assign(Object.create(MaybeProto), {
        __value
    });



    const add = _.curry((a, value) => a + value);
    const match = _.curry((a, value) => a.test(value));


    console.log("Maybe", Maybe('Malkovich Malkovich').map(match(/va/ig)));
//=> Maybe(['a', 'a'])

    console.log("Maybe", Maybe(null).map(match(/a/ig)));
//=> Maybe(null)

    const testc = compose(_.prop('age'), trace("test"), add(10));

    console.log("Maybe", Maybe({
        name: 'Boris',
    }).map(testc));
//=> Maybe(null)

    console.log("Maybe", Maybe({
        name: 'Dinah',
        age: 14,
    }).map(testc));


    //The Stampit example from Eric Elliots blog https://medium.com/javascript-scene/3-different-kinds-of-prototypal-inheritance-es6-edition-32d777fa16c9#.8nqk9omvd

    const availability = init(function () {
        let isOpen = false; // private

        Object.assign(this, {
            open () {
                isOpen = true;
                return this;
            },
            close () {
                isOpen = false;
                return this;
            },
            isOpen () {
                return isOpen;
            }
        });
    });

// Here's a stamp with public methods, and some state:
    const membership = compose({
        methods: {
            addMember (member) {
                this.members[member.name] = member;
                return this;
            },
            getMember (name) {
                return this.members[name];
            }
        },
        properties: {
            members: {}
        }
    });

// Let's set some defaults:
    const defaults = compose({
        init({name, specials}) {
            this.name = name || this.name;
            this.specials = specials || this.specials;
        },
        properties: {
            name: 'The Saloon',
            specials: 'Whisky, Gin, Tequila'
        }
    });

    const overrides = init(function (overrides) {
        Object.assign(this, overrides);
    });

// Classical inheritance has nothing on this. No parent/child coupling.
// No deep inheritance hierarchies.
// Just good, clean code reusability.
    const bar = compose(membership, defaults, overrides, availability);
    const myBar = bar({name: 'Moe\'s'});

// Silly, but proves that everything is as it should be.
    const result = myBar.addMember({name: 'Homer'}).open().getMember('Homer');
    console.log(result); // { name: 'Homer' }
    console.log(`
  name: ${ myBar.name }
  isOpen: ${ myBar.isOpen() }
  specials: ${ myBar.specials }
`);


    // so using Oclass we're recreating what stampit does


    const OLOOavailability = function (privateValue) {
        // private
        let isOpen = privateValue;

        return public = {
            isOpen: () => isOpen,
            open: function () {
                isOpen = true
                return this;
            },
            close: function () {
                isOpen = false;
                return this;
            }
        };
    };

    const OLOOmembershipProterties = function (){
        return {
            members: {},

        };
    }

    const OLOOdefaultProperties = function (nameValue, specialsValue) {
        return {
            name: nameValue ? nameValue : 'The Saloon',
            specials: specialsValue ? specialsValue : 'Whisky, Gin, Tequila'
        }
    };

    // lump them altogether

    const OLOOParam = (privateValue, nameValue, specialsValue) => Object.assign({}, OLOOavailability(privateValue), OLOOmembershipProterties(), OLOOdefaultProperties(nameValue, specialsValue));
    //

    //create  protoypes
    const OLOOProto = function () {
        return {
            addMember (member) {
                this.members[member.name] = member;
                return this;
            },
            getMember (name) {
                return this.members[name];
            }
        };
    };


    //(target,source) assign the parameter(source) to the prototypes (target)

    const OLOOStampit = (privateValue, nameValue, specialsValue) => Object.assign(Object.create(OLOOProto()), OLOOParam.call(this, privateValue, nameValue, specialsValue));

    const mybarOLOO = OLOOStampit(false, "Moe's");

    //the target prototypes dont get overwritten doing it this way
    const OLOOStampitFlip = (privateValue, nameValue, specialsValue) => Object.assign(OLOOParam.call(this, privateValue, nameValue, specialsValue), Object.create(OLOOProto()));

    const mybarOLOOflip = OLOOStampitFlip(false, "Moe's");


    const OLOOStampitResult = mybarOLOO.addMember({name: 'Homer'}).open().getMember('Homer');

    console.log(OLOOStampitResult); // { name: 'Homer' }
    console.log(`OLOOStampit
  name: ${ mybarOLOO.name }
  isOpen: ${ mybarOLOO.isOpen() }
  specials: ${ mybarOLOO.specials }
`);

}
